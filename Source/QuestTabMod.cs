﻿using Harmony;
using System;
using System.Reflection;
using UnityEngine;
using Verse;

namespace Merthsoft.QuestTab {
    [StaticConstructorOnStartup]
    public class QuestTabMod : Mod {
        public static HarmonyInstance HarmonyInstance { get; }
        
        public static QuestTabModSettings Settings { get; private set; }

        public override string SettingsCategory() => "Quest Tab";

        static QuestTabMod() {
            HarmonyInstance = HarmonyInstance.Create("Merthsoft.QuestTab.QuestTabMod");
            HarmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
        }

        public QuestTabMod(ModContentPack content) : base(content) {
            if ((Settings = GetSettings<QuestTabModSettings>()) == null) {
                Log.Error("Unable to load QuestTabModSettings.");
            }

            Compatability.MoreFactionInteraction.Load();
        }

        public override void DoSettingsWindowContents(Rect inRect) {
            Listing_Standard ls = new Listing_Standard();
            ls.Begin(inRect);

            ls.CheckboxLabeled("Hide tab when not on the world menu.", ref Settings.HideTabWhenNotOnWorldMenu);

            ls.End();
            Settings.Write();
        }
    }
}
