﻿using System;
using System.Reflection;

namespace Merthsoft.QuestTab.Compatability {
    public static class MoreFactionInteraction {
        public const string AssemblyName = "MoreFactionInteraction";
        public const string SettlementBumperCropCompTypeName = "MoreFactionInteraction.World_Incidents.WorldObjectComp_SettlementBumperCropComp";

        public static Type SettlementBumperCropComp { get; private set; }

        public static void Load() {
            try {
                var assembly = Assembly.Load(AssemblyName);
                if (assembly == null) {
                    return;
                }

                SettlementBumperCropComp = assembly.GetType(SettlementBumperCropCompTypeName);
            } catch {
                return;
            }
        }
    }
}
