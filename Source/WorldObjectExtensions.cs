﻿using Merthsoft.QuestTab.Compatability;
using RimWorld.Planet;
using Verse;
using static Merthsoft.QuestTab.MainTabWindow.QuestTabWindow;

namespace Merthsoft.QuestTab {
	public static class WorldObjectExtensions {
		public static object GetSortKey(this WorldObject worldObject, SortMode mode) {
			switch (mode) {
				case SortMode.Alphabetical:
					return worldObject.GetInstancePropertyWithFallback("QuestTab_Label", worldObject.LabelCap);
				case SortMode.ExpirationTime:
					var timeoutComp = worldObject.GetComponent<TimeoutComp>();
					if (timeoutComp != null) { return timeoutComp.TicksLeft; }

					var tradeRequestComp = worldObject.GetComponent<TradeRequestComp>();
					if (tradeRequestComp != null) { return tradeRequestComp.TicksLeft(); }

					var settlementBumperCropComp = worldObject.GetMfiBumperCropComp();
					if (settlementBumperCropComp != null) { return settlementBumperCropComp.MfiBumperCropComp_TicksLeft(); }

					return worldObject.GetInstancePropertyWithFallback("QuestTab_TicksLeft", -1);
				case SortMode.Hostility:
					return worldObject.GetInstancePropertyWithFallback("QuestTab_Hostility",
						worldObject.Faction == null ? -1000 : worldObject.Faction == Find.FactionManager.OfPlayer ? 1000 : worldObject.Faction.GoodwillWith(Find.FactionManager.OfPlayer));
				default:
					return worldObject.ID;
			}
		}

		public static WorldObjectComp GetMfiBumperCropComp(this WorldObject worldObject)
			=> MoreFactionInteraction.SettlementBumperCropComp == null ? null : worldObject.GetComponent(MoreFactionInteraction.SettlementBumperCropComp);

		public static bool IsQuest(this WorldObject worldObject) {
			if (worldObject.GetComponent<TimeoutComp>() != null) { return worldObject.def.defName != SetUpCamp.AbandonedCampDefName; }
			if (worldObject.GetComponent<TradeRequestComp>()?.ActiveRequest ?? false) { return true; }
			if (worldObject.GetMfiBumperCropComp()?.MfiBumperCropComp_ActiveRequest() ?? false) { return true; }

			return worldObject.GetInstancePropertyWithFallback("QuestTab_IsQuest", false);
		}

		public static string GetQuestLabel(this WorldObject worldObject) {
			if (worldObject.GetComponent<TimeoutComp>() != null) { return worldObject.LabelCap; }
			if (worldObject.GetComponent<TradeRequestComp>()?.ActiveRequest ?? false) { return "LetterLabelCaravanRequest".Translate(); }
			if (worldObject.GetMfiBumperCropComp()?.MfiBumperCropComp_ActiveRequest() ?? false) { return "MFI_LetterLabel_HarvestRequest".Translate(); }

			return worldObject.GetInstancePropertyWithFallback("QuestTab_Label", "Unknown Quest");
		}
	}
}
