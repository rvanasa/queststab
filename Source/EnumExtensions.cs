﻿using Merthsoft.QuestTab.MainTabWindow;
using System.Collections.Generic;
using Verse;
using static Merthsoft.QuestTab.MainTabWindow.QuestTabWindow;

namespace Merthsoft.QuestTab {
    public static class EnumExtensions {
        public static string GetLabel(this QuestTabType type) {
            switch (type) {
                case QuestTabType.Quests:
                    return "Quests";
                case QuestTabType.Bases:
                    return "Bases";
                case QuestTabType.AllWorldObjects:
                    return "All World Objects";
                default:
                    return "Unknown";
            }
        }

        public static string GetLabel(this SortMode mode) {
            switch (mode) {
                case SortMode.Alphabetical:
                    return "Alphabetical";
                case SortMode.ExpirationTime:
                    return "Expiration Time";
                case SortMode.Hostility:
                    return "Hostility";
                default:
                    return "Unknown";
            }
        }

        public static string GetLabel(this SortDirection direction) => direction.ToString();

        public static FloatMenuOption ToFloatMenuOption(this QuestTabType type, QuestTabWindow window)
            => new FloatMenuOption(type.GetLabel(), () => window.TabType = type);

        public static Widgets.DropdownMenuElement<QuestTabType> ToDropDownMenuElement(this QuestTabType type, QuestTabWindow window)
            => new Widgets.DropdownMenuElement<QuestTabType>() { option = type.ToFloatMenuOption(window), payload = type };

        public static FloatMenuOption ToFloatMenuOption(this SortMode mode, QuestTabWindow window)
            => new FloatMenuOption(mode.GetLabel(), () => window.Sort = mode);

        public static Widgets.DropdownMenuElement<SortMode> ToDropDownMenuElement(this SortMode mode, QuestTabWindow window)
            => new Widgets.DropdownMenuElement<SortMode>() { option = mode.ToFloatMenuOption(window), payload = mode };

        public static FloatMenuOption ToFloatMenuOption(this SortDirection direction, QuestTabWindow window)
            => new FloatMenuOption(direction.GetLabel(), () => window.Direction = direction);

        public static Widgets.DropdownMenuElement<SortDirection> ToDropDownMenuElement(this SortDirection direction, QuestTabWindow window)
            => new Widgets.DropdownMenuElement<SortDirection>() { option = direction.ToFloatMenuOption(window), payload = direction };

        public static HashSet<SortMode> GetSupportedSortModes(this QuestTabType type)
            => SupportedSortModes[type];
    }
}
