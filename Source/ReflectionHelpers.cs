﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Merthsoft.QuestTab {
	public static class ReflectionHelpers {
		static BindingFlags fieldFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.GetField | BindingFlags.GetProperty;

		public static T GetInstancePropertyWithFallback<T>(this object instance, string fieldName, T fallback = default) {
			var type = instance.GetType();
			var property = type.GetProperty(fieldName, fieldFlags);
			return (T)(property != null ? property.GetValue(instance, null) : fallback);
		}

		public static object GetInstanceProperty(this object instance, string fieldName) {
			var type = instance.GetType();
			var property = type.GetProperty(fieldName, fieldFlags);
			return property.GetValue(instance, null);
		}

		public static object GetInstanceField(this object instance, string fieldName) {
			var type = instance.GetType();
			var field = type.GetField(fieldName, fieldFlags);
			return field.GetValue(instance);
		}

		public static T GetInstanceField<T>(this object instance, string fieldName) where T : class {
			var type = instance.GetType();
			var field = type.GetField(fieldName, fieldFlags);
			return field.GetValue(instance) as T;
		}
	}
}
